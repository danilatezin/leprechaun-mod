package ru.cdssiolutions.leprechaun.tabs;

import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import ru.cdssiolutions.leprechaun.Leprechaun;
import ru.cdssiolutions.leprechaun.utils.RegistryHandler;

public class MainTab extends CreativeModeTab {

    public static final MainTab INSTANCE = new MainTab(CreativeModeTab.TABS.length, "Leprechaun");

    public MainTab(int index, String label) {
        super(index, label);
    }

    @Override
    public ItemStack makeIcon() {
        return new ItemStack(RegistryHandler.RAINBOWS.get());
    }
}
