package ru.cdssiolutions.leprechaun;

import net.minecraft.client.gui.screens.social.PlayerEntry;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber
public class EventHandler {
    boolean renderHelloMessage = true;


    @SubscribeEvent
    public void onJoin(EntityJoinWorldEvent event){
        if (renderHelloMessage && event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            player.sendMessage(
                    new TextComponent("Hello my new friends, %d".replace("%d", player.getName().getString())),
                    null);
            renderHelloMessage = false;
        }

    }
}
