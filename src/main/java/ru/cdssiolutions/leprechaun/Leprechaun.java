package ru.cdssiolutions.leprechaun;

import com.mojang.logging.LogUtils;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import ru.cdssiolutions.leprechaun.utils.RegistryHandler;
import org.slf4j.Logger;

@Mod(Leprechaun.MOD_ID)
public class Leprechaun {

    private static final Logger LOGGER = LogUtils.getLogger();
    public static final String MOD_ID = "leprechaun";

    public Leprechaun() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        MinecraftForge.EVENT_BUS.register(this);
        MinecraftForge.EVENT_BUS.register(new EventHandler());
        RegistryHandler.init();
    }

    private void setup(final FMLCommonSetupEvent event) {

    }

}
