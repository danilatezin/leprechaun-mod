package ru.cdssiolutions.leprechaun.utils;

import net.minecraft.world.item.Item;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import ru.cdssiolutions.leprechaun.Leprechaun;
import ru.cdssiolutions.leprechaun.items.Rainbows;
import ru.cdssiolutions.leprechaun.tabs.MainTab;

public class RegistryHandler {

    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, Leprechaun.MOD_ID);

    //This registers things

    public static final RegistryObject<Item> RAINBOWS = ITEMS.register("rainbows",
            ()-> new Rainbows(
                    new Item.Properties()
                            .tab(MainTab.INSTANCE)
            ));


    //Final register
    public static void init(){
        ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());

    }
}
